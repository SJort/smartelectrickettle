void setup(){
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(3, OUTPUT);
}

void loop(){
  int input = analogRead(A1);
  Serial.println(input);
  int min = 530;
  int max = 570;
  if(input < min){
    input = min;
  }
  else if(input > max){
    input = max;
  }
  int output = map(input, min, max, 0, 255);
  //String lmao = "Mapped " + (String)input + " to " + (String)output;
  //Serial.println(lmao);
  analogWrite(3, output);
  delay(1);
}
