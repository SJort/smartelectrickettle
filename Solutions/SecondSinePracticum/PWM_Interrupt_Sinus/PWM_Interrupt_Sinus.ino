#include <math.h>
#define TABLE_LENGTH 512
int index; 
char sin_table[TABLE_LENGTH];

void setup() {
  for (index = 0; index < TABLE_LENGTH; index++) {
    sin_table[index] = (int)((sin(index*2*3.1415/TABLE_LENGTH)+1)*120);
  }
  noInterrupts();
  pinMode(11,OUTPUT);   //OC2A pwm output
  index = 0;

  // Init timer2 for fast pwm mode
  TCCR2A = 0b10100011;
  TCCR2B = 0x01;    //PWM frequency = 8kHz (16MHz/1/256 = 62500Hz)
  OCR2A = 0;
  OCR2B = 0;
  //enable timer overflow interrupt
  TIMSK2 = 0x01;
  //enable OCB interrupt
  TIMSK2 |= 0x02;
  index = 0;
  interrupts();
}

/* Timer2 interrupt routine */
ISR( TIMER2_OVF_vect) {
  PORTB &= ~0x10;
  PORTB |= 0x020;

  OCR2A = sin_table[index];
  
  index++;
  if (index == TABLE_LENGTH) {
    index = 0;
  }
}

ISR( TIMER2_COMPA_vect ) {
  PORTB &= ~0x20;
  PORTB |= 0x10;
}

void loop() {
}
