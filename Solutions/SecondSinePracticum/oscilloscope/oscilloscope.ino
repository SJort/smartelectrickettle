#define FREQ 60 // sampling frequency in Hz

void setupTimerInterrupt(int freq) {
  cli(); //stop interrupts

//set timer1 interrupt
  TCCR1A = 0; // set entire TCCR1A register to 0
  TCCR1B = 0; // same for TCCR1B
  TCNT1  = 0; // initialize counter value to 0
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // set compare match register for increments based on freq
  OCR1A = 15625 / freq - 1; // = 16 * 10^6 (Arduino clock freq) / 1024 / freq - 1
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei(); // allow interrupts
}

ISR(TIMER1_COMPA_vect) {
  Serial.println(analogRead(0));
}

void setup() {
  Serial.begin(115200);
  setupTimerInterrupt(FREQ);
}

void loop() {
}
