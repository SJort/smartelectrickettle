#define tonePin 3

void setup(){
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(tonePin, OUTPUT);
}

void loop(){
  int input = analogRead(A1);
  int output = map(input, 0, 1023, 0, 5);

  double prank = 0;

  switch(output){
    case 0:{
      prank = 262*2;
      break; 
    }
    case 1:{
      prank = 440*2;
      break; 
    }
    case 2:{
      prank = 699*2;
      break; 
    }
    case 3:{
      prank = 1047*2;
      break; 
    }
    case 4:{
      prank = 2093*2;
      break; 
    }
  }

  int prank2 = (int)((1/prank)*1000000);
  Serial.println(prank2);
  digitalWrite(tonePin, HIGH);
  delayMicroseconds(prank2);
  digitalWrite(tonePin, LOW);
  delayMicroseconds(prank2);
}
