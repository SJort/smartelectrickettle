#define tonePin 3

void setup(){
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(tonePin, OUTPUT);
}

void loop(){
  int input = analogRead(A1);
  int output = map(input, 0, 1023, 0, 5);

  int prank = 0;

  switch(output){
    case 0:{
      prank = 50;
      break; 
    }
    case 1:{
      prank = 40;
      break; 
    }
    case 2:{
      prank = 30;
      break; 
    }
    case 3:{
      prank = 20;
      break; 
    }
    case 4:{
      prank = 10;
      break; 
    }
  }
  
  digitalWrite(tonePin, HIGH);
  delayMicroseconds(prank);
  digitalWrite(tonePin, LOW);
  delayMicroseconds(prank);
}
