void setup(){
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(3, OUTPUT);
}

void loop(){
  int input = analogRead(A0);
  int output = map(input, 0, 2013, 0, 255);
  analogWrite(3, output);
  delay(1);
}
