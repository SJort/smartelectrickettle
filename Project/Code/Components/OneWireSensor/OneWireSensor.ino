#include <OneWire.h>
#include <DallasTemperature.h>
#define sensorPin 7

OneWire oneWire(sensorPin);
DallasTemperature sensors(&oneWire);

void setup() {
  Serial.begin(9600);
  Serial.println("Started Sensory");
  sensors.begin();
}

void loop() {
  sensors.requestTemperatures();
  Serial.print("Temperature: ");
  Serial.print(sensors.getTempCByIndex(0)); //0 to access the first sensor in the bus
  Serial.println("°C");
  delay(100);
}
