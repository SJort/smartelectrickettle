#define plusButton 2
#define minusButton 3

void setup() {
  pinMode(plusButton, INPUT_PULLUP);
  pinMode(minusButton, INPUT_PULLUP);

  Serial.begin(9600);
  Serial.println("Started");

}

boolean plusButtonPressed;
boolean minusButtonPressed;

void loop() {
  plusButtonPressed = !digitalRead(plusButton);
  minusButtonPressed = !digitalRead(minusButton);

  if (plusButtonPressed) {
    Serial.println("PlusButton pressed!");
  }

  if (minusButtonPressed) {
    Serial.println("MinusButton pressed!");
  }

  if (plusButtonPressed || minusButtonPressed) {
    delay(200);
  }
}
