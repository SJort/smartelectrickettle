#define relayPin 7

void setup() {
  Serial.begin(9600);
  Serial.println("Started FeedBackLoop");
  pinMode(relayPin, OUTPUT);
}

void loop() {
  digitalWrite(relayPin, HIGH);
  delay(200);
  digitalWrite(relayPin, LOW);
  delay(200);
}
