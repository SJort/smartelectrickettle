#define plusButton 20
#define minusButton 21
#define buttonPressDelay 250

int currentTemperature = 100;
int modus;

void setup() {
  Serial.begin(9600);
  Serial.println("Started buttons");
  pinMode(plusButton, INPUT_PULLUP);
  pinMode(minusButton, INPUT_PULLUP);
}

void displayTemperature() {
  Serial.print("Temperature: "); Serial.println(currentTemperature);
}

void displayModus() {
  Serial.print("Modus: "); Serial.println(modus);
}

void checkButtons() {
  if (plusButtonPressed() || minusButtonPressed()) {
    if (plusButtonPressed()) {
      delay(100);
      if (minusButtonPressed()) {
        increaseModus();
      }
      else {
        currentTemperature++;
        displayTemperature();
      }
    }

    else if (minusButtonPressed()) {
    delay(100);
      if (plusButtonPressed()) {
        increaseModus();
      }
      else {
        currentTemperature--;
        displayTemperature();
      }
    }

    delay(200);
  }

}

void loop() {
  checkButtons();
}

void increaseModus() {
  if (modus == 2) {
    modus = 0;
  }
  else {
    modus++;
  }
  displayModus();
}

boolean plusButtonPressed() {
  return !digitalRead(plusButton);
}

boolean minusButtonPressed() {
  return !digitalRead(minusButton);
}
