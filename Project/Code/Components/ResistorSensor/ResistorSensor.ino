  /* Guido Ponson
   Hogeschool Rotterdam
   TI2B, 2018
*/

//analog pin where we read the voltage in the voltage devider
int readPin = 0;
//int to store this reading
int readVolt;
//precise value of our known 10k resistor
float R10k = 10070.0;
//unknown resistance of sensor which we will calculate
float Rtemp;
//value to store log of read resistance
float logRtemp;
//contants for Steinhart-Hart equation
float c1 = 1.009249522e-03;
float c2 = 2.378405444e-04;
float c3 = 2.019202697e-07;

void setup() {
  Serial.begin(9600);
  Serial.println("start");
}


void loop() {
  float result = calculate(readRes());
  Serial.print(", temperature: " );
  Serial.println(result);
  delay(100);
}

float calculate(float Rtemp) {
  //return steinhart-Hart equation of our contants with Resistance of our sensor
  Serial.print("Rtemp: ");
  Serial.print(Rtemp);
  return (1.0 / (c1 + c2 * log(Rtemp) + c3 * pow(log(Rtemp), 3.0)) - 273.15);
}

float readRes() {
  readVolt = analogRead(readPin);
  /*formula for the resistance of Rtemp is
    Rtemp = R10k((inputVolt/readVolt)-1)
    where inputVolt is 5 volt or represented as
    1023 by the 10 bit arduino (2^10 -1) (-1 because
    of 0 value).
  */


  Rtemp = R10k * ((1023.0 / float(readVolt)) - 1.0);
  /*Serial.println();
    Serial.print("Rtemp: ");
    Serial.print(Rtemp);
    Serial.print("     Volt: ");
    Serial.print(readVolt);
  */
  return Rtemp;
}
