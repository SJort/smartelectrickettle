#define plusButton 8
#define minusButton 9
#define sensorPin 7
#define relayPin 6
#define resistorSensorPin A0


//analog pin where we read the voltage in the voltage devider
int readPin = 0;
//int to store this reading
int readVolt;
//precise value of our known 10k resistor
float R10k = 10000.0;
//unknown resistance of sensor which we will calculate
float Rtemp;
//value to store log of read resistance
float logRtemp;
//contants for Steinhart-Hart equation
float c1 = 1.009249522e-03;
float c2 = 2.378405444e-04;
float c3 = 2.019202697e-07;

int desiredTemperature = 20;
float currentTemperature;
int lastTemperature;
int modus;
String temperatureString;
String modusString;

boolean hasReachedDesiredTemperature;
int counter;

//library for the library
#include <OneWire.h>
OneWire oneWire(sensorPin);

//library for the unused sensor
#include <DallasTemperature.h>
DallasTemperature sensors(&oneWire);

#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#define BACKLIGHT 3
//LiquidCrystal_I2C lcd(I2C_ADDR, En_pin, Rw_pin, Rs_pin, D4_pin, D5_pin, D6_pin, D7_pin);
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);


void setup() {
  sensors.begin();

  pinMode(relayPin, OUTPUT);
  pinMode(plusButton, INPUT_PULLUP);
  pinMode(minusButton, INPUT_PULLUP);

  //start and refresh lcd so device doesn't start with empty screen
  lcd.begin(16, 2);
  lcd.setBacklightPin(BACKLIGHT, POSITIVE);
  lcd.setBacklight(HIGH);
  retrieveTemperatures();
  refreshLCD();

  Serial.begin(9600);
  Serial.println("Started SmartElectricKettle");
}

void refreshLCD() {
  //concat the top temperature screen to prevent having to set the cursor
  temperatureString = "Degrees: ";
  temperatureString.concat((int)currentTemperature);
  temperatureString.concat("/");
  temperatureString.concat(desiredTemperature);

  //translate the mode to a readable string
  modusString = "Mode: ";
  switch (modus) {
    case 0: {
        modusString.concat("Disabled");
        break;
      }
    case 1: {
        modusString.concat("Heat&Stop");
        break;
      }
    case 2: {
        modusString.concat("Heat&Hold");
        break;
      }
    case 3: {
        modusString.concat("Enabled");
        break;
      }
  }
  //clear lcd to remove all remaining chars
  lcd.clear();
  lcd.print(temperatureString);
  //bottom line
  lcd.setCursor(0, 1);
  lcd.print(modusString);
}

//method to check the buttons and handle them if pressed
void checkButtons() {
  //if a button is pressed
  if (plusButtonPressed() || minusButtonPressed()) {
    if (plusButtonPressed()) {
      //wait a little so the user doesn't have to press both buttons at the exact same millisecond
      delay(100);
      if (minusButtonPressed()) {
        //if in that time the other button is pressed change the modus
        increaseModus();
      }
      else {
        //otherwise user just wanted to set a new temperature
        desiredTemperature++;
      }
    }

    else if (minusButtonPressed()) {
      delay(100);
      if (plusButtonPressed()) {
        increaseModus();
      }
      else {
        desiredTemperature--;
      }
    }

    //refresh lcd with new values
    refreshLCD();
    delay(200);

    //refresh variables used in the heat&stop mode
    hasReachedDesiredTemperature = false;
    counter = 0;
  }
}


void loop() {
  checkButtons();
  handleSensors();
}


void handleSensors() {
  retrieveTemperatures();
  switch (modus) {
    case 0: {
        //disabled
        setRelayEnabled(false);
        break;
      }
    case 1: {
        //heat&stop
        if (hasReachedDesiredTemperature) {
          //do nothing if desired temperature is already reached
          setRelayEnabled(false);
          break;
        }
        else {
          setRelayEnabled(true);
        }
        if (currentTemperature > desiredTemperature) {
          counter++;
          if (counter > 5) {
            //set this so it stops when reached
            hasReachedDesiredTemperature = true;
          }
        }
        else {
          counter = 0;
        }
        break;
      }
    case 2: {
        //heat&hold
        if (currentTemperature < desiredTemperature) {
          setRelayEnabled(true);
        }
        else {
          setRelayEnabled(false);
        }
        break;
      }
    case 3: {
        //enabled
        setRelayEnabled(true);
        break;
      }
  }
}

//seperate method for if you have to change relays and they work differenly
void setRelayEnabled(boolean enabled) {
  digitalWrite(relayPin, enabled);
}

//method to read temperatures from sensors
void retrieveTemperatures() {
  sensors.requestTemperatures();
  currentTemperature = sensors.getTempCByIndex(0);
  //if the linear sensor returns a false value aka isnt connected/hasn't enough power
  if(currentTemperature == -127){
    currentTemperature = calculate(readRes());
  }
  
  //casting because otherwise the lcd refreshes for every miniscule temperature change and it also wouldn't fit on lcd
  if ((int)currentTemperature != lastTemperature) {
    refreshLCD();
    lastTemperature = currentTemperature;
  }
}

float calculate(float Rtemp) {
  //return steinhart-Hart equation of our contants with Resistance of our sensor
  return ((1.0 / (c1 + (c2 * log(Rtemp)) + (c3 * pow(log(Rtemp), 3.0)))) - 273.15);
}

float readRes() {
  readVolt = analogRead(readPin);
  /*formula for the resistance of Rtemp is
    Rtemp = R10k((inputVolt/readVolt)-1)
    where inputVolt is 5 volt or represented as
    1023 by the 10 bit arduino (2^10 -1) (-1 because
    of 0 value).
  */
  Rtemp = R10k * ((1023.0 / float(readVolt)) - 1.0);
  return Rtemp;
}

//method to loop through the modi
void increaseModus() {
  if (modus == 3) {
    modus = 0;
  }
  else {
    modus++;
  }
}


boolean plusButtonPressed() {
  return !digitalRead(plusButton);
}

boolean minusButtonPressed() {
  return !digitalRead(minusButton);
}
